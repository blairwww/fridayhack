# README #

## Creative Project on E-Commerce Analysis ##

The data comes from the Wish platform.

We analyzed the product and sales information on E-commerce summer clothing.

Various data visualization and modeling techniques were applied for finding interesting facts and generating business insights.  


## Challenge Task on Logfile ##
Analyse an apache log file (in /Apache_log) and get statistics from it, e.g.

a. Produce a report in Microsoft excel (xlsx) or csv showing

b. Errors, notices, forbidden access, most common & least common functions

c. Plot some graphs showing the servers usage profile.
